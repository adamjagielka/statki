package statki;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Statki extends JPanel {

    int Shooting;
    private JTable PanelPlayer;
    private JTable PanelEnemy;
    private JTextArea Info;
    private TableModels EnemyModel;
    private int Score;
    //PanelPlayer Ships
    private int AllPlayerShips;
    private int AllEnemyShips;
    private int Quantity1;
    private int Quantity2;
    private int Quantity3;
    private int Quantity4;
    private int Quantity5;
    //PanelEnemy Ships
    private int Quantity11;
    private int Quantity22;
    private int Quantity33;
    private int Quantity44;
    private int Quantity55;
    private String Water;
    private String Undefined;
    private String Hit;
    private String Miss;
    private String Ship1;
    private String Ship2;
    private String Ship3;
    private String Ship4;
    private String Ship5;

    public Statki() {
        try {
            FileWriter fw1 = new FileWriter("ranking.txt");
            fw1.append("nick");
        } catch (IOException ex) {
            Logger.getLogger(Statki.class.getName()).log(Level.SEVERE, null, ex);
        }
        InitComponents();
        Setters();
        NewGame();
    }

    private void PanelPlayerMouseClicked() {
        ManualShips(PanelPlayer);
    }

    private void PanelEnemyMouseClicked() {
        if (PanelEnemy.getSelectedRow() > 1 && PanelEnemy.getSelectedColumn() > 1) {
            if (Shooting <= 0) {
                Shoot(PanelEnemy, "manual");
               /* try {
                    Thread.sleep(100);

                } catch (InterruptedException ex) {
                    Logger.getLogger(Statki.class.getName()).log(Level.SEVERE, null, ex);
                }  */
                Shoot(PanelPlayer, "auto");
             /*  try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Statki.class.getName()).log(Level.SEVERE, null, ex);
                } */
                Info.append("\nGreen is shooting!!!");
            }
        }
    }

    public void Setters() {
        if (getWater() != null) {
            setWater(getWater());
            setUndefined(getUndefined());
            setHit(getHit());
            setMiss(getMiss());
            setShip1(getShip1());
            setShip2(getShip2());
            setShip3(getShip3());
            setShip4(getShip4());
            setShip5(getShip5());
/// Player Ships
            setQuantity1(getQuantity1());
            setQuantity2(getQuantity2());
            setQuantity3(getQuantity3());
            setQuantity4(getQuantity4());
            setQuantity5(getQuantity5());
/// Enemy Ships
            setQuantity11(getQuantity11());
            setQuantity22(getQuantity22());
            setQuantity33(getQuantity33());
            setQuantity44(getQuantity44());
            setQuantity55(getQuantity55());
            setColor("White");  //"Black" or "White"
        } else {
            setWater("~");
            setUndefined("?");
            setHit("x");
            setMiss(".");
            setShip1("||");
            setShip2("||");
            setShip3("||");
            setShip4("||");
            setShip5("||");
/// Player Ships
            setQuantity1(1);
            setQuantity2(0);
            setQuantity3(0);
            setQuantity4(0);
            setQuantity5(0);
/// Enemy Ships
            setQuantity11(0);
            setQuantity22(0);
            setQuantity33(0);
            setQuantity44(0);
            setQuantity55(1);
            setColor("White");  //"Black" or "White"
        }
    }

    public void NewGame() {
        Shooting = 1;
        Score = 100;
        RandomShips(getQuantity11(), 1);
        RandomShips(getQuantity22(), 2);
        RandomShips(getQuantity33(), 3);
        RandomShips(getQuantity44(), 4);
        RandomShips(getQuantity55(), 5);
        AllPlayerShips = getQuantity1() + 2 * getQuantity2() + 3 * getQuantity3() + 4 * getQuantity4() + 5 * getQuantity5();
        AllEnemyShips = getQuantity11() + 2 * getQuantity22() + 3 * getQuantity33() + 4 * getQuantity44() + 5 * getQuantity55();
        setQuantity11(0);
        setQuantity22(0);
        setQuantity33(0);
        setQuantity44(0);
        setQuantity55(0);
    }

    void InitComponents() {
        EnemyModel = new TableModels();
        Info = new JTextArea();
        PanelPlayer = new JTable();
        PanelEnemy = new JTable();
        setLayout(new GridLayout(1, 2));
        add(PanelPlayer);
        add(Info);
        add(PanelEnemy);
        PanelPlayer.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PanelPlayerMouseClicked();
            }
        });
        PanelEnemy.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PanelEnemyMouseClicked();
            }
        });
        PanelPlayer.setFont(new Font("Arial", Font.BOLD, 25));
        PanelEnemy.setFont(new Font("Arial", Font.BOLD, 25));
        PanelPlayer.setRowHeight(40);
        PanelEnemy.setRowHeight(40);
        PanelPlayer.setRowSelectionAllowed(false);
        PanelEnemy.setRowSelectionAllowed(false);
    }

    void RandomShips(int Quantity, int Size) {
        Random Roww = new Random();
        Random Columnn = new Random();
        double a;
        double b;
        for (int i = 1; i <= Quantity; i++) {
            a = Roww.nextDouble();
            b = Columnn.nextDouble();
            b = b * 10 + 2;
            switch (Size) {
                case 1:
                    a = a * 10 + 2;
                    if (EnemyModel.getValueAt((int) a, (int) b).equals(0)) {
                        EnemyModel.setValueAt(Size, (int) a, (int) b);
                    } else {
                        i--;
                        break;
                    }
                    for (int ii = -1, jj = -1; jj < 2; ii++) {
                        if (EnemyModel.getValueAt((int) a + ii, (int) b + jj).equals("*") || EnemyModel.getValueAt((int) a + ii, (int) b + jj).equals(0)) {
                            EnemyModel.setValueAt("*", (int) a + ii, (int) b + jj);
                        }
                        if (ii == 1) {
                            ii = -2;
                            jj++;
                        }
                    }
                    break;
                case 2:
                    a = a * 9 + 2;
                    if (EnemyModel.getValueAt((int) a, (int) b).equals(0) && EnemyModel.getValueAt((int) a + 1, (int) b).equals(0)) {
                        EnemyModel.setValueAt(Size, (int) a, (int) b);
                        EnemyModel.setValueAt(Size, (int) a + 1, (int) b);
                    } else {
                        i--;
                        break;
                    }
                    for (int ii = -1, jj = -1; jj < 2; ii++) {
                        if (EnemyModel.getValueAt((int) a + ii, (int) b + jj).equals("*") || EnemyModel.getValueAt((int) a + ii, (int) b + jj).equals(0)) {
                            EnemyModel.setValueAt("*", (int) a + ii, (int) b + jj);
                        }
                        if (ii == 2) {
                            ii = -2;
                            jj++;
                        }
                    }
                    break;
                case 3:
                    a = a * 8 + 2;
                    if (EnemyModel.getValueAt((int) a, (int) b).equals(0) && EnemyModel.getValueAt((int) a + 1, (int) b).equals(0)
                            && EnemyModel.getValueAt((int) a + 2, (int) b).equals(0)) {
                        EnemyModel.setValueAt(Size, (int) a, (int) b);
                        EnemyModel.setValueAt(Size, (int) a + 1, (int) b);
                    } else {
                        i--;
                        break;
                    }
                    EnemyModel.setValueAt(Size, (int) a, (int) b);
                    EnemyModel.setValueAt(Size, (int) a + 1, (int) b);
                    EnemyModel.setValueAt(Size, (int) a + 2, (int) b);
                    for (int ii = -1, jj = -1; jj < 2; ii++) {
                        if (EnemyModel.getValueAt((int) a + ii, (int) b + jj).equals("*") || EnemyModel.getValueAt((int) a + ii, (int) b + jj).equals(0)) {
                            EnemyModel.setValueAt("*", (int) a + ii, (int) b + jj);
                        }
                        if (ii == 3) {
                            ii = -2;
                            jj++;
                        }
                    }
                    break;
                case 4:
                    a = a * 7 + 2;
                    if (EnemyModel.getValueAt((int) a, (int) b).equals(0) && EnemyModel.getValueAt((int) a + 1, (int) b).equals(0)
                            && EnemyModel.getValueAt((int) a + 2, (int) b).equals(0) && EnemyModel.getValueAt((int) a + 3, (int) b).equals(0)) {
                        EnemyModel.setValueAt(Size, (int) a, (int) b);
                        EnemyModel.setValueAt(Size, (int) a + 1, (int) b);
                    } else {
                        i--;
                        break;
                    }
                    EnemyModel.setValueAt(Size, (int) a, (int) b);
                    EnemyModel.setValueAt(Size, (int) a + 1, (int) b);
                    EnemyModel.setValueAt(Size, (int) a + 2, (int) b);
                    EnemyModel.setValueAt(Size, (int) a + 3, (int) b);
                    for (int ii = -1, jj = -1; jj < 2; ii++) {
                        if (EnemyModel.getValueAt((int) a + ii, (int) b + jj).equals("*") || EnemyModel.getValueAt((int) a + ii, (int) b + jj).equals(0)) {
                            EnemyModel.setValueAt("*", (int) a + ii, (int) b + jj);
                        }
                        if (ii == 4) {
                            ii = -2;
                            jj++;
                        }
                    }
                    break;
                case 5:
                    a = a * 6 + 2;
                    if (EnemyModel.getValueAt((int) a, (int) b).equals(0) && EnemyModel.getValueAt((int) a + 1, (int) b).equals(0)
                            && EnemyModel.getValueAt((int) a + 2, (int) b).equals(0) && EnemyModel.getValueAt((int) a + 3, (int) b).equals(0)
                            && EnemyModel.getValueAt((int) a + 4, (int) b).equals(0)) {
                        EnemyModel.setValueAt(Size, (int) a, (int) b);
                        EnemyModel.setValueAt(Size, (int) a + 1, (int) b);
                    } else {
                        i--;
                        break;
                    }
                    EnemyModel.setValueAt(Size, (int) a, (int) b);
                    EnemyModel.setValueAt(Size, (int) a + 1, (int) b);
                    EnemyModel.setValueAt(Size, (int) a + 2, (int) b);
                    EnemyModel.setValueAt(Size, (int) a + 3, (int) b);
                    EnemyModel.setValueAt(Size, (int) a + 4, (int) b);
                    for (int ii = -1, jj = -1; jj < 2; ii++) {
                        if (EnemyModel.getValueAt((int) a + ii, (int) b + jj).equals("*") || EnemyModel.getValueAt((int) a + ii, (int) b + jj).equals(0)) {
                            EnemyModel.setValueAt("*", (int) a + ii, (int) b + jj);
                        }
                        if (ii == 5) {
                            ii = -2;
                            jj++;
                        }
                    }
                    break;
            }
        }
    }

    public void Shoot(JTable Panel, String Mode) {
        int Row = Panel.getSelectedRow();
        int Column = Panel.getSelectedColumn();
        String ValueOf;
        if (Mode.equals("auto")) {
            double row = new Random().nextDouble();
            double column = new Random().nextDouble();
            row = row * 10 + 2;
            column = column * 10 + 2;
            ValueOf = Panel.getValueAt((int) row, (int) column).toString();
            if (ValueOf.equals(getShip1()) || ValueOf.equals(getShip2()) || ValueOf.equals(getShip3()) || ValueOf.equals(getShip4()) || ValueOf.equals(getShip5())) {
                Info.setText(" trafiłeś");
                Panel.setValueAt(getHit(), (int) row, (int) column);
                System.out.println(Panel.getValueAt(Panel.getSelectedRow(), Panel.getSelectedColumn()));
                AllPlayerShips--;
                System.out.println(AllPlayerShips);
                if (AllPlayerShips <= 0) {
                    Info.setText("Przeciwnik wygrał");
                    JOptionPane.showConfirmDialog(new JFrame(), "Przeciwnik wygrał");
                    NewGame();
                }
            } else if (ValueOf.equals(getMiss()) || ValueOf.equals(getHit())) {
                System.out.println("tu już strzelano");
                Shoot(PanelPlayer, "auto");
            } else {
                Info.setText(" miss");
                Panel.setValueAt(getMiss(), (int) row, (int) column);
            }
        } else if (Mode.equals("manual")) {
            ValueOf = EnemyModel.getValueAt(Panel.getSelectedRow(), Panel.getSelectedColumn()).toString();
            if (Shooting <= 0) {
                Info.setText(" Shooting time");
                if (ValueOf.equals("1") || ValueOf.equals("2") || ValueOf.equals("3") || ValueOf.equals("4") || ValueOf.equals("5")) {
                    Info.setText(" trafiłeś");
                    Panel.setValueAt(getHit(), Row, Column);
                    System.out.println(Panel.getValueAt(Panel.getSelectedRow(), Panel.getSelectedColumn()));
                    AllEnemyShips--;
                    if (AllEnemyShips <= 0) {
                        Info.setText("Gracz wygrał");
                        JOptionPane.showConfirmDialog(new JFrame(), "Gracz wygrał, Twój wynik to " + Score + " punktów");
                        NewGame();
                    }
                } else if (PanelEnemy.getValueAt(Row, Column).equals(getMiss()) || PanelEnemy.getValueAt(Row, Column).equals(getHit())) {
                    System.out.println("tu już strzelano");
                    try {
                        PanelEnemy.clearSelection();
                        Shoot(PanelEnemy, "manual");
                        PanelEnemy.clearSelection();
                    } catch (Exception e) {
                        System.out.println("error");
                    }
                } else {
                    Info.setText(" miss");
                    Panel.setValueAt(getMiss(), Row, Column);
                    Score--;
                }
            }
        }
    }

    public final void ManualShips(JTable Panel/*, int Quantity1, int Quantity2, int Quantity3, int Quantity4, int Quantity5*/) {
        int Column = Panel.getSelectedColumn();
        int Row = Panel.getSelectedRow();
        if (Panel == PanelPlayer) {
            if (this.Quantity1 == 0 && this.Quantity2 == 0 && this.Quantity3 == 0 && this.Quantity4 == 0 && this.Quantity5 == 0) {
                Info.setText("Rozmieszczono wszystkie statki!");
            } else if (Panel.getValueAt(Row, Column).equals(1)) {
                Info.setText("Tu już jest statek!");
            } else if (this.Quantity1 > 0 && Panel.getValueAt(Row, Column).equals(getWater())) {
                this.Quantity1 = this.Quantity1 - 1;
                Info.setText("umieszczono statek na " + (Row - 1) + " " + (Column - 1));
                Info(PanelPlayer);
                Panel.setValueAt(getShip1(), Row, Column);
                for (int i = -1, j = -1; j < 2; i++) {
                    try {
                        if (Panel.getValueAt(Row + i, Column + j).equals("*") || Panel.getValueAt(Row + i, Column + j).equals(getWater())) {
                            Panel.setValueAt("*", Row + i, Column + j);
                        }
                    } catch (Exception e) {
                        System.out.println("error");
                    }
                    if (i == 1) {
                        i = -2;
                        j++;
                    }
                }
            } else if (this.Quantity2 > 0 && Panel.getValueAt(Row, Column).equals(getWater()) && Panel.getValueAt(Row + 1, Column).equals(getWater())) {
                this.Quantity2 = this.Quantity2 - 1;
                Info.setText("umieszczono statek na " + (Row - 1) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + Row + " " + (Column - 1));
                Info(PanelPlayer);
                Panel.setValueAt(getShip2(), Row, Column);
                Panel.setValueAt(getShip2(), Row + 1, Column);
                for (int i = -1, j = -1; j < 2; i++) {
                    try {
                        if (Panel.getValueAt(Row + i, Column + j).equals("*") || Panel.getValueAt(Row + i, Column + j).equals(getWater())) {
                            Panel.setValueAt("*", Row + i, Column + j);
                        }
                    } catch (Exception e) {
                        System.out.println("error");
                    }
                    if (i == 2) {
                        i = -2;
                        j++;
                    }
                }
            } else if (this.Quantity3 > 0 && Panel.getValueAt(Row, Column).equals(getWater())
                    && Panel.getValueAt(Row + 1, Column).equals(getWater()) && Panel.getValueAt(Row + 2, Column).equals(getWater())) {
                this.Quantity3 = this.Quantity3 - 1;
                Info.setText("umieszczono statek na " + (Row - 1) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row + 1) + " " + (Column - 1));
                Info(PanelPlayer);
                Panel.setValueAt(getShip3(), Row, Column);
                Panel.setValueAt(getShip3(), Row + 1, Column);
                Panel.setValueAt(getShip3(), Row + 2, Column);
                for (int i = -1, j = -1; j < 2; i++) {
                    try {
                        if (Panel.getValueAt(Row + i, Column + j).equals("*") || Panel.getValueAt(Row + i, Column + j).equals(getWater())) {
                            Panel.setValueAt("*", Row + i, Column + j);
                        }
                    } catch (Exception e) {
                        System.out.println("error");
                    }
                    if (i == 3) {
                        i = -2;
                        j++;
                    }
                }
            } else if (this.Quantity4 > 0 && Panel.getValueAt(Row, Column).equals(getWater()) && Panel.getValueAt(Row + 1, Column).equals(getWater())
                    && Panel.getValueAt(Row + 2, Column).equals(getWater()) && Panel.getValueAt(Row + 3, Column).equals(getWater())) {
                this.Quantity4 = this.Quantity4 - 1;
                Info.setText("umieszczono statek na " + (Row - 1) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row + 1) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row + 2) + " " + (Column - 1));
                Info(PanelPlayer);
                Panel.setValueAt(getShip4(), (Row), (Column));
                Panel.setValueAt(getShip4(), (Row + 1), (Column));
                Panel.setValueAt(getShip4(), Row + 2, (Column));
                Panel.setValueAt(getShip4(), Row + 3, (Column));
                for (int i = -1, j = -1; j < 2; i++) {
                    if (Panel.getValueAt(Row + i, Column + j).equals("*") || Panel.getValueAt(Row + i, Column + j).equals(getWater())) {
                        Panel.setValueAt("*", Row + i, Column + j);
                    }
                    if (i == 4) {
                        i = -2;
                        j++;
                    }
                }
            } else if (this.Quantity5 > 0 && Panel.getValueAt(Row, Column).equals(getWater()) && Panel.getValueAt(Row + 1, Column).equals(getWater())
                    && Panel.getValueAt(Row + 2, Column).equals(getWater()) && Panel.getValueAt(Row + 3, Column).equals(getWater())
                    && Panel.getValueAt(Row + 4, Column).equals(getWater())) {
                this.Quantity5 = this.Quantity5 - 1;
                Info.setText("umieszczono statek na " + (Row - 1) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row + 1) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row + 2) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row + 3) + " " + (Column - 1));
                Info(PanelPlayer);
                Panel.setValueAt(getShip5(), Row, Column);
                Panel.setValueAt(getShip5(), Row + 1, Column);
                Panel.setValueAt(getShip5(), Row + 2, Column);
                Panel.setValueAt(getShip5(), Row + 3, Column);
                Panel.setValueAt(getShip5(), Row + 4, Column);
                for (int i = -1, j = -1; j < 2; i++) {
                    if (Panel.getValueAt(Row + i, Column + j).equals("*") || Panel.getValueAt(Row + i, Column + j).equals(getWater())) {
                        Panel.setValueAt("*", Row + i, Column + j);
                    }
                    if (i == 5) {
                        i = -2;
                        j++;
                    }
                }
            }
        } else if (Panel == PanelEnemy) {
            if (this.Quantity11 == 0 && this.Quantity22 == 0 && this.Quantity33 == 0 && this.Quantity44 == 0 && this.Quantity55 == 0) {
                Info.setText("Rozmieszczono wszystkie statki!");
            } else if (Panel.getValueAt(Row, Column).equals(1)) {
                Info.setText("Tu już jest statek!");
            } else if (this.Quantity11 > 0 && Panel.getValueAt(Row, Column).equals(0)) {
                this.Quantity11 = this.Quantity11 - 1;
                Info.setText("umieszczono statek na " + (Row - 1) + " " + (Column - 1));
                Info(PanelEnemy);
                Panel.setValueAt(1, Row, Column);
                for (int i = -1, j = -1; j < 2; i++) {
                    if (Panel.getValueAt(Row + i, Column + j).equals("*") || Panel.getValueAt(Row + i, Column + j).equals(0)) {
                        Panel.setValueAt("*", Row + i, Column + j);
                    }
                    if (i == 1) {
                        i = -2;
                        j++;
                    }
                }
            } else if (this.Quantity22 > 0 && Panel.getValueAt(Row, Column).equals(0) && Panel.getValueAt(Row + 1, Column).equals(0)) {
                this.Quantity22 = this.Quantity22 - 1;
                Info.setText("umieszczono statek na " + (Row - 1) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row) + " " + (Column - 1));
                Info(PanelEnemy);
                Panel.setValueAt(2, (Row), (Column));
                Panel.setValueAt(2, (Row + 1), (Column));
                for (int i = -1, j = -1; j < 2; i++) {
                    try {
                        if (Panel.getValueAt(Row + i, Column + j).equals("*") || Panel.getValueAt(Row + i, Column + j).equals(0)) {
                            Panel.setValueAt("*", Row + i, Column + j);
                        }
                    } catch (Exception e) {
                        System.out.println("error");
                    }
                    if (i == 2) {
                        i = -2;
                        j++;
                    }
                }
            } else if (this.Quantity33 > 0 && Panel.getValueAt(Row, Column).equals(0) && Panel.getValueAt(Row + 1, Column).equals(0)
                    && Panel.getValueAt(Row + 2, Column).equals(0)) {
                this.Quantity33 = this.Quantity33 - 1;
                Info.setText("numieszczono statek na " + (Row - 1) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row + 1) + " " + (Column - 1));
                Info(PanelEnemy);
                Panel.setValueAt(3, Row, Column);
                Panel.setValueAt(3, Row + 1, Column);
                Panel.setValueAt(3, Row + 2, Column);
                for (int i = -1, j = -1; j < 2; i++) {
                    if (Panel.getValueAt(Row + i, Column + j).equals("*") || Panel.getValueAt(Row + i, Column + j).equals(0)) {
                        Panel.setValueAt("*", Row + i, Column + j);
                    }
                    if (i == 3) {
                        i = -2;
                        j++;
                    }
                }
            } else if (this.Quantity44 > 0 && Panel.getValueAt(Row, Column).equals(0) && Panel.getValueAt(Row + 1, Column).equals(0)
                    && Panel.getValueAt(Row + 2, Column).equals(0) && Panel.getValueAt(Row + 3, Column).equals(0)) {
                this.Quantity44 = this.Quantity44 - 1;
                Info.setText("umieszczono statek na " + (Row - 1) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row + 1) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row + 2) + " " + (Column - 1));
                Info(PanelEnemy);
                Panel.setValueAt(4, Row, Column);
                Panel.setValueAt(4, Row + 1, Column);
                Panel.setValueAt(4, Row + 2, Column);
                Panel.setValueAt(4, Row + 3, Column);
                for (int i = -1, j = -1; j < 2; i++) {
                    if (Panel.getValueAt(Row + i, Column + j).equals("*") || Panel.getValueAt(Row + i, Column + j).equals(0)) {
                        Panel.setValueAt("*", Row + i, Column + j);
                    }
                    if (i == 4) {
                        i = -2;
                        j++;
                    }
                }
            } else if (this.Quantity55 > 0 && Panel.getValueAt(Row, Column).equals(0) && Panel.getValueAt(Row + 1, Column).equals(0)
                    && Panel.getValueAt(Row + 2, Column).equals(0) && Panel.getValueAt(Row + 3, Column).equals(0)
                    && Panel.getValueAt(Row + 4, Column).equals(0)) {
                this.Quantity55 = this.Quantity55 - 1;
                Info.setText("umieszczono statek na " + (Row - 1) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row + 1) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row + 2) + " " + (Column - 1));
                Info.append("\numieszczono statek na " + (Row + 3) + " " + (Column - 1));
                Info(PanelEnemy);
                Panel.setValueAt(5, Row, Column);
                Panel.setValueAt(5, Row + 1, Column);
                Panel.setValueAt(5, Row + 2, Column);
                Panel.setValueAt(5, Row + 3, Column);
                Panel.setValueAt(5, Row + 4, Column);
                for (int i = -1, j = -1; j < 2; i++) {
                    if (Panel.getValueAt(Row + i, Column + j).equals("*") || Panel.getValueAt(Row + i, Column + j).equals(0)) {
                        Panel.setValueAt("*", Row + i, Column + j);
                    }
                    if (i == 5) {
                        i = -2;
                        j++;
                    }
                }
            }
        }
        if (Quantity1 + Quantity2 + Quantity3 + Quantity4 + Quantity5 + Quantity11 + Quantity22 + Quantity33 + Quantity44 + Quantity55 == 0) {
            Shooting--;
        }
    }

    void Info(JTable Panel) {
        System.out.println(Panel.getSelectedColumn());
        System.out.println(Panel.getSelectedRow());
        if (Panel == PanelPlayer) {
            Info.append("\nZostało do rozmieszczenia [1] " + Quantity1);
            Info.append("\nZostało do rozmieszczenia [2] " + Quantity2);
            Info.append("\nZostało do rozmieszczenia [3] " + Quantity3);
            Info.append("\nZostało do rozmieszczenia [4] " + Quantity4);
            Info.append("\nZostało do rozmieszczenia [5] " + Quantity5);
        } else if (Panel == PanelEnemy) {
            Info.append("\nZostało do rozmieszczenia [1] " + Quantity11);
            Info.append("\nZostało do rozmieszczenia [2] " + Quantity22);
            Info.append("\nZostało do rozmieszczenia [3] " + Quantity33);
            Info.append("\nZostało do rozmieszczenia [4] " + Quantity44);
            Info.append("\nZostało do rozmieszczenia [5] " + Quantity55);
        }
    }

    //////////////////////////////////////////////////
    public int getQuantity1() {
        return Quantity1;
    }

    public void setQuantity1(int Quantity1) {
        this.Quantity1 = Quantity1;
    }

    public int getQuantity2() {
        return Quantity2;
    }

    public void setQuantity2(int Quantity2) {
        this.Quantity2 = Quantity2;
    }

    public int getQuantity3() {
        return Quantity3;
    }

    public void setQuantity3(int Quantity3) {
        this.Quantity3 = Quantity3;
    }

    public int getQuantity4() {
        return Quantity3;
    }

    public void setQuantity4(int Quantity4) {
        this.Quantity4 = Quantity4;
    }

    public int getQuantity5() {
        return Quantity5;
    }

    public void setQuantity5(int Quantity5) {
        this.Quantity5 = Quantity5;
    }

    public int getQuantity11() {
        return Quantity11;
    }

    public void setQuantity11(int Quantity11) {
        this.Quantity11 = Quantity11;
    }

    public int getQuantity22() {
        return Quantity22;
    }

    public void setQuantity22(int Quantity22) {
        this.Quantity22 = Quantity22;
    }

    public int getQuantity33() {
        return Quantity33;
    }

    public void setQuantity33(int Quantity33) {
        this.Quantity33 = Quantity33;
    }

    public int getQuantity44() {
        return Quantity44;
    }

    public void setQuantity44(int Quantity44) {
        this.Quantity44 = Quantity44;
    }

    public int getQuantity55() {
        return Quantity55;
    }

    public void setQuantity55(int Quantity55) {
        this.Quantity55 = Quantity55;
    }

    public String getUndefined() {
        return Undefined;
    }

    public void setUndefined(String Undefined) {
        this.Undefined = Undefined;
        PanelEnemy.setModel(new javax.swing.table.DefaultTableModel(
                                    new Object[][]{
                                            {"", "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", ""},
                                            {"", "", "", "", "", "", "", "", "", "", "", "", ""},
                                            {"1", "", Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, ""},
                                            {"2", "", Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, ""},
                                            {"3", "", Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, ""},
                                            {"4", "", Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, ""},
                                            {"5", "", Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, ""},
                                            {"6", "", Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, ""},
                                            {"7", "", Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, ""},
                                            {"8", "", Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, ""},
                                            {"9", "", Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, ""},
                                            {"10", "", Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, Undefined, ""},
                                            {"", "", "", "", "", "", "", "", "", "", "", "", ""},},
                                    new String[]{
                                            "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6",
                                            "Title 7", "Title 8", "Title 9", "Title 10", "Title 11", "Title 12", "Title 13"
                                    }
                            ) {
                                final boolean[] canEdit = new boolean[]{
                                        false, false, false, false, false, false, false, false, false, false, false, false, false
                                };

                                @Override
                                public boolean isCellEditable(int rowIndex, int columnIndex) {
                                    return canEdit[columnIndex];
                                }
                            }
        );
    }

    public String getWater() {
        return Water;
    }

    public void setWater(String Water) {
        this.Water = Water;
        PanelPlayer.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{
                        {"", "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", ""},
                        {"", "", "", "", "", "", "", "", "", "", "", "", ""},
                        {"1", "", getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), ""},
                        {"2", "", getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), ""},
                        {"3", "", getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), ""},
                        {"4", "", getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), ""},
                        {"5", "", getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), ""},
                        {"6", "", getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), ""},
                        {"7", "", getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), ""},
                        {"8", "", getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), ""},
                        {"9", "", getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), ""},
                        {"10", "", getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), getWater(), ""},
                        {"", "", "", "", "", "", "", "", "", "", "", "", ""},},
                new String[]{
                        "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6",
                        "Title 7", "Title 8", "Title 9", "Title 10", "Title 11", "Title 12", "Title 13"
                }
        ) {
            final boolean[] canEdit = new boolean[]{
                    false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
    }

    public String getHit() {
        return Hit;
    }

    public void setHit(String Hit) {
        this.Hit = Hit;
    }

    public String getMiss() {
        return Miss;
    }

    public void setMiss(String Miss) {
        this.Miss = Miss;
    }

    public String getShip1() {
        return Ship1;
    }

    public void setShip1(String Ship1) {
        this.Ship1 = Ship1;
    }

    public String getShip2() {
        return Ship2;
    }

    public void setShip2(String Ship2) {
        this.Ship2 = Ship2;
    }

    public String getShip3() {
        return Ship3;
    }

    public void setShip3(String Ship3) {
        this.Ship3 = Ship3;
    }

    public String getShip4() {
        return Ship4;
    }

    public void setShip4(String Ship4) {
        this.Ship4 = Ship4;
    }

    public String getShip5() {
        return Ship5;
    }

    public void setShip5(String Ship5) {
        this.Ship5 = Ship5;
    }

    public void setColor(String Color) {
        switch (Color) {
            case "Black":
                Info.setText("Changed color to black");
                PanelPlayer.setForeground(new Color(0, 200, 0));
                PanelPlayer.setBackground(new Color(0, 0, 0));
                PanelEnemy.setForeground(new Color(255, 0, 0));
                PanelEnemy.setBackground(new Color(0, 0, 0));
                Info.setForeground(new Color(255, 255, 255));
                Info.setBackground(new Color(0, 0, 0));
                break;
            case "White":
                Info.setText("Changed color to white");
                PanelPlayer.setForeground(new Color(0, 200, 0));
                PanelPlayer.setBackground(new Color(255, 255, 255));
                PanelEnemy.setForeground(new Color(200, 0, 0));
                PanelEnemy.setBackground(new Color(255, 255, 255));
                Info.setForeground(new Color(0, 0, 0));
                Info.setBackground(new Color(255, 255, 255));
                break;
            default:
                Info.setText("error, color not found");
                break;
        }
    }
}

class TableModels extends AbstractTableModel {
    // TableModel's column names

    private final String[] columnNames = {
            "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6",
            "Title 7", "Title 8", "Title 9", "Title 10", "Title 11", "Title 12", "Title 13"
    };

    // TableModel's data
    private final Object[][] data = {
            {"", "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", ""},
            {"", "", "", "", "", "", "", "", "", "", "", "", ""},
            {"1", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ""},
            {"2", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ""},
            {"3", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ""},
            {"4", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ""},
            {"5", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ""},
            {"6", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ""},
            {"7", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ""},
            {"8", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ""},
            {"9", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ""},
            {"10", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ""},
            {"", "", "", "", "", "", "", "", "", "", "", "", ""},};

    /**
     * Returns the number of rows in the table model.
     */
    @Override
    public int getRowCount() {
        return data.length;
    }

    /**
     * Returns the number of columns in the table model.
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * Returns the column name for the column index.
     */
    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    /**
     * Returns data type of the column specified by its index.
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    /**
     * Returns the value of a table model at the specified row index and column
     * index.
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return data[rowIndex][columnIndex];
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        data[rowIndex][columnIndex] = aValue;
    }
}

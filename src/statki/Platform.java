/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statki;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Random;

/**
 * @author adam8
 */
public class Platform extends JPanel {

    private final JButton przywroc;
    private final JButton zapiszWyglad;
    private final JButton zapiszIlosc;
    private final Dimension screenSize;
    private final Point center;
    private final JTextArea jTextArea1 = new JTextArea();
    JButton jButton1 = new JButton("FaceBean off");
    JButton jButton2 = new JButton("Temperatura losowa");
    JButton jButton3 = new JButton("Wyświetl zmienne");
    JButton jButton4 = new JButton("Facebean on");
    JButton jButton5 = new JButton("Temperatura górna błąd");
    JButton jButton6 = new JButton("Temperatura górna");
    JButton jButton7 = new JButton("Temperatura dolna");
    JButton jButton8 = new JButton("Temperatura dolna błąd");
    JButton jButton9 = new JButton("Przywróć domyślne");
    JButton jButton10 = new JButton("Temperatura pobrana");
    JSpinner jSpinner1 = new JSpinner();
    JSpinner jSpinner2 = new JSpinner();
    JSpinner jSpinner3 = new JSpinner();
    JSpinner jSpinner4 = new JSpinner();
    JSpinner jSpinner5 = new JSpinner();
    private Statki statki;
    private KolkoKrzyzyk kolkoKrzyzyk;
    private TextPanel symulator;
    private JFrame jFrameDostosujStatki;
    private JTextField jTexTFieldsetWater;
    private JTextField jTexTFieldsetUndefined;
    private JTextField jTexTFieldsetHit;
    private JTextField jTexTFieldsetMiss;
    private JTextField jTexTFieldsetShip1;
    private JTextField jTexTFieldsetShip2;
    private JTextField jTexTFieldsetShip3;
    private JTextField jTexTFieldsetShip4;
    private JTextField jTexTFieldsetShip5;
    private JSpinner jTexTFieldsetQuantity1;
    private JSpinner jTexTFieldsetQuantity2;
    private JSpinner jTexTFieldsetQuantity3;
    private JSpinner jTexTFieldsetQuantity4;
    private JSpinner jTexTFieldsetQuantity5;
    private JSpinner jTexTFieldsetQuantity11;
    private JSpinner jTexTFieldsetQuantity22;
    private JSpinner jTexTFieldsetQuantity33;
    private JSpinner jTexTFieldsetQuantity44;
    private JSpinner jTexTFieldsetQuantity55;

    public Platform() throws InterruptedException {
        center = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        JMenuBar jMenuBar1;
        screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLayout(new BorderLayout());
        setSize(new Dimension(screenSize.width, screenSize.height));
        setPreferredSize(new Dimension(screenSize.width, screenSize.height));
        statki = new Statki();
        statki.setVisible(false);
        jMenuBar1 = new JMenuBar();
        statki.setVisible(false);
        kolkoKrzyzyk = new KolkoKrzyzyk();
        kolkoKrzyzyk.setVisible(false);
        symulator = new TextPanel();
        symulator.setVisible(false);
        JMenu jMenuNowaGra = new JMenu("Nowa gra");
        JMenuItem jMenuItemNowaGraStatki = new JMenuItem("Statki");
        JMenuItem jMenuItemNowaGraSudoku = new JMenuItem("Sudoku");
        JMenuItem jMenuItemNowaGraKiK = new JMenuItem("Kółko i krzyżyk");
        JMenuItem jMenuItemNowaGraKiKMulti = new JMenuItem("Kółko i krzyżyk");
        JMenuItem jMenuItemNowaGraSymulator = new JMenuItem("Symulator 2020/2021");
        JMenu jMenuOnePlayer = new JMenu("1 gracz");
        JMenu jMenuMultiPlayer = new JMenu("2 graczy");
        jMenuNowaGra.add(jMenuOnePlayer);
        jMenuOnePlayer.add(jMenuItemNowaGraSudoku);
        jMenuOnePlayer.add(jMenuItemNowaGraKiK);
        jMenuOnePlayer.add(jMenuItemNowaGraStatki);
        jMenuOnePlayer.add(jMenuItemNowaGraSymulator);
        jMenuNowaGra.add(jMenuMultiPlayer);
        jMenuMultiPlayer.add(jMenuItemNowaGraKiKMulti);
        jMenuBar1.add(jMenuNowaGra);
        JMenu jMenuOptions = new JMenu("opcje");
        JMenu jMenuGames = new JMenu("Wybór gry");
        JMenuItem jMenuItemStatki = new JMenuItem("Statki");
        JMenuItem jMenuItemSudoku = new JMenuItem("Sudoku");
        JMenuItem jMenuItemKiK = new JMenuItem("Kółko i krzyżyk");
        JMenuItem jMenuItemSymulator = new JMenuItem("Symulator 2020/2021");
        JMenu jMenuMotyw = new JMenu("Motyw");
        JMenuItem jMenuItemCiemny = new JMenuItem("Ciemny");
        JMenuItem jMenuItemJasny = new JMenuItem("Jasny");
        JMenu jMenuTabela = new JMenu("Tabela wyników");
        JMenuItem jMenuItemTabelaStatki = new JMenuItem("Statki");
        JMenuItem jMenuItemTabelaSudoku = new JMenuItem("Sudoku");
        JMenuItem jMenuItemTabelaKiK = new JMenuItem("Kółko i krzyżyk");
        JMenu jMenuDostosuj = new JMenu("Dostosuj");
        JMenu jMenuDostosujSudoku = new JMenu("Sudoku");
        JMenu jMenuDostosujStatki = new JMenu("Statki");
        JMenuItem jMenuItemDostosujSymulator = new JMenuItem("Symulator 2020/2021");
        JMenuItem jMenuItemDostosujSudoku = new JMenuItem("Sudoku");
        JMenu jMenuDostosujKiK = new JMenu("Kółko i krzyżyk");
        JMenuItem jMenuItemDostosujKiKNazwa = new JMenuItem("Nazwa gracza");
        JMenuItem jMenuItemDostosujKiKZnak = new JMenuItem("Wybierz znak");
        zapiszWyglad = new JButton("Zapisz");
        zapiszIlosc = new JButton("Zapisz");
        przywroc = new JButton("Przywróć domyślne");
        JMenuItem jMenuItemDostosujStatkiIlosc = new JMenuItem("Ilość statków");
        JMenuItem jMenuItemDostosujStatkiWyglad = new JMenuItem("Wygląd");
        SpinnerNumberModel StandardModel = new SpinnerNumberModel();
        StandardModel.setMaximum(5);
        StandardModel.setMinimum(0);
        StandardModel.setValue(1);
        jMenuGames.add(jMenuItemStatki);
        jMenuGames.add(jMenuItemSudoku);
        jMenuGames.add(jMenuItemKiK);
        jMenuGames.add(jMenuItemSymulator);
        jMenuOptions.add(jMenuMotyw);
        jMenuOptions.add(jMenuDostosuj);
        jMenuMotyw.add(jMenuItemCiemny);
        jMenuMotyw.add(jMenuItemJasny);
        jMenuTabela.add(jMenuItemTabelaStatki);
        jMenuTabela.add(jMenuItemTabelaSudoku);
        jMenuTabela.add(jMenuItemTabelaKiK);
        jMenuDostosuj.add(jMenuDostosujStatki);
        jMenuDostosujStatki.add(jMenuItemDostosujStatkiIlosc);
        jMenuDostosujStatki.add(jMenuItemDostosujStatkiWyglad);
        jMenuDostosuj.add(jMenuDostosujSudoku);
        jMenuDostosuj.add(jMenuDostosujKiK);
        jMenuDostosujKiK.add(jMenuItemDostosujKiKNazwa);
        jMenuDostosujKiK.add(jMenuItemDostosujKiKZnak);
        jMenuDostosuj.add(jMenuItemDostosujSymulator);
        jMenuBar1.add(jMenuGames);
        jMenuBar1.add(jMenuTabela);
        jMenuBar1.add(jMenuOptions);
        add(jMenuBar1, BorderLayout.NORTH);
        jMenuItemStatki.addActionListener(this::jMenuItemStatkiActionPerformed);
        jMenuItemSudoku.addActionListener(this::jMenuItemSudokuActionPerformed);
        jMenuItemKiK.addActionListener(this::jMenuItemKiKActionPerformed);
        jMenuItemSymulator.addActionListener(this::jMenuItemSymulatorActionPerformed);
        jMenuItemCiemny.addActionListener(this::jMenuItemCiemnyActionPerformed);
        jMenuItemJasny.addActionListener(this::jMenuItemJasnyActionPerformed);
        jMenuItemDostosujStatkiIlosc.addActionListener(this::jMenuItemDostosujStatkiIloscActionPerformed);
        jMenuItemDostosujStatkiWyglad.addActionListener(this::jMenuItemDostosujStatkiWygladActionPerformed);
        jMenuItemDostosujSudoku.addActionListener(this::jMenuItemDostosujSudokuActionPerformed);
        jMenuItemDostosujKiKNazwa.addActionListener(this::jMenuItemDostosujKiKNazwaActionPerformed);
        jMenuItemDostosujKiKZnak.addActionListener(this::jMenuItemDostosujKiKZnakActionPerformed);
        jMenuItemDostosujSymulator.addActionListener(this::jMenuItemDostosujSymulatorActionPerformed);
        zapiszWyglad.addActionListener(this::zapiszWygladActionPerformed);
        zapiszIlosc.addActionListener(this::zapiszIloscActionPerformed);
        przywroc.addActionListener(this::przywrocActionPerformed);
        jMenuItemNowaGraStatki.addActionListener(this::jMenuItemNowaGraStatkiActionPerformed);
        jMenuItemNowaGraKiK.addActionListener(this::jMenuItemNowaGraKiKActionPerformed);
        jMenuItemNowaGraKiKMulti.addActionListener(this::jMenuItemNowaGraKiKMultiActionPerformed);
        jMenuItemTabelaKiK.addActionListener(this::jMenuItemTabelaKiKActionPerformed);
        jMenuItemNowaGraSudoku.addActionListener(this::jMenuItemNowaGraSudokuActionPerformed);
        jMenuItemNowaGraSymulator.addActionListener(this::jMenuItemNowaGraSymulatorActionPerformed);
        jButton1.setText("FaceBean off");
        jButton1.addActionListener(this::jButton1ActionPerformed);
        jButton2.setText("Temperatura losowa");
        jButton2.addActionListener(this::jButton2ActionPerformed);
        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        //jScrollPane1.setViewportView(jTextArea1);
        jButton3.setText("Wyświetl zmienne");
        jButton3.addActionListener(this::jButton3ActionPerformed);
        jButton4.setText("FaceBean on");
        jButton4.addActionListener(this::jButton4ActionPerformed);
        jSpinner1.setModel(new javax.swing.SpinnerNumberModel(0.0d, null, null, 0.5d));
        jSpinner2.setModel(new javax.swing.SpinnerNumberModel(0.0d, null, null, 0.5d));
        jSpinner3.setModel(new javax.swing.SpinnerNumberModel(0.0d, null, null, 0.5d));
        jButton5.setText("Temperatura górna błąd");
        jButton5.addActionListener(this::jButton5ActionPerformed);
        jButton6.setText("Temperatura górna");
        jButton6.addActionListener(this::jButton6ActionPerformed);
        jButton7.setText("Temperatura dolna");
        jButton7.addActionListener(this::jButton7ActionPerformed);
        jButton8.setText("Temperatura dolna błąd");
        jButton8.addActionListener(this::jButton8ActionPerformed);
        //    jLabel1.setText("                    Ustaw zmienne");
        jButton9.setText("Przywróć domyślne");
        jButton9.addActionListener(this::jButton9ActionPerformed);
        jButton10.setText("Temperatura pobrana");
        jButton10.addActionListener(this::jButton10ActionPerformed);
        jSpinner5.setModel(new javax.swing.SpinnerNumberModel(0.0d, null, null, 0.5d));
    }

    private void jMenuItemStatkiActionPerformed(ActionEvent evt) {
        remove(kolkoKrzyzyk);
        remove(symulator);
        remove(kolkoKrzyzyk);
        add(statki, BorderLayout.CENTER);
        statki.setVisible(true);
        System.out.println("uruchomiono statki ");
        repaint();
    }

    private void jMenuItemSudokuActionPerformed(ActionEvent evt) {
        remove(statki);
        remove(symulator);
        remove(kolkoKrzyzyk);
        System.out.println("uruchomiono sudoku ");
        repaint();
    }

    private void jMenuItemKiKActionPerformed(ActionEvent evt) {
        add(kolkoKrzyzyk, BorderLayout.CENTER);
        kolkoKrzyzyk.setVisible(true);
        remove(statki);
        remove(symulator);
        System.out.println("uruchomiono kółko i krzyżyk ");
        repaint();
    }

    private void jMenuItemSymulatorActionPerformed(ActionEvent evt) {
        add(symulator, BorderLayout.CENTER);
        symulator.setVisible(true);
        remove(kolkoKrzyzyk);
        remove(statki);
        System.out.println("uruchomiono symulator ");
        repaint();
    }

    private void jMenuItemCiemnyActionPerformed(ActionEvent evt) {
        statki.setColor("Black");
        kolkoKrzyzyk.setColor("Black");
        symulator.setColor("Black");
        repaint();
    }

    private void jMenuItemJasnyActionPerformed(ActionEvent evt) {
        statki.setColor("White");
        kolkoKrzyzyk.setColor("White");
        symulator.setColor("White");
        repaint();
    }

    private void przywrocActionPerformed(ActionEvent evt) {
        // statki.Setters();
        statki.setWater("~");
        statki.setUndefined("?");
        statki.setHit("x");
        statki.setMiss(".");
        statki.setShip1("||");
        statki.setShip2("||");
        statki.setShip3("||");
        statki.setShip4("||");
        statki.setShip5("||");
/// Player Ships
        statki.setQuantity1(1);
        statki.setQuantity2(0);
        statki.setQuantity3(0);
        statki.setQuantity4(0);
        statki.setQuantity5(0);
/// Enemy Ships
        statki.setQuantity11(0);
        statki.setQuantity22(0);
        statki.setQuantity33(0);
        statki.setQuantity44(0);
        statki.setQuantity55(1);
        jTexTFieldsetQuantity1.setValue(statki.getQuantity1());
        jTexTFieldsetQuantity2.setValue(statki.getQuantity2());
        jTexTFieldsetQuantity3.setValue(statki.getQuantity3());
        jTexTFieldsetQuantity4.setValue(statki.getQuantity4());
        jTexTFieldsetQuantity5.setValue(statki.getQuantity5());
        jTexTFieldsetQuantity11.setValue(statki.getQuantity11());
        jTexTFieldsetQuantity22.setValue(statki.getQuantity22());
        jTexTFieldsetQuantity33.setValue(statki.getQuantity33());
        jTexTFieldsetQuantity44.setValue(statki.getQuantity44());
        jTexTFieldsetQuantity55.setValue(statki.getQuantity55());
        jTexTFieldsetShip1.setText(statki.getShip1());
        jTexTFieldsetShip2.setText(statki.getShip2());
        jTexTFieldsetShip3.setText(statki.getShip3());
        jTexTFieldsetShip4.setText(statki.getShip4());
        jTexTFieldsetShip5.setText(statki.getShip5());
        jTexTFieldsetHit.setText(statki.getHit());
        jTexTFieldsetMiss.setText(statki.getMiss());
        jTexTFieldsetUndefined.setText(statki.getUndefined());
        jTexTFieldsetWater.setText(statki.getWater());
    }

    private void jMenuItemNowaGraStatkiActionPerformed(ActionEvent evt) {
        remove(statki);
        remove(kolkoKrzyzyk);
        remove(symulator);
        this.statki = new Statki();
        add(statki, BorderLayout.CENTER);
        statki.setVisible(true);
        revalidate();
    }

    private void jMenuItemNowaGraSudokuActionPerformed(ActionEvent evt) {
        remove(kolkoKrzyzyk);
        remove(statki);
        remove(symulator);
        repaint();
    }

    private void jMenuItemNowaGraKiKMultiActionPerformed(ActionEvent evt) {
        remove(kolkoKrzyzyk);
        remove(statki);
        remove(symulator);
        kolkoKrzyzyk.setSingleMode(false);
        this.kolkoKrzyzyk = new KolkoKrzyzyk();
        kolkoKrzyzyk.setSingleMode(false);
        add(kolkoKrzyzyk, BorderLayout.CENTER);
        kolkoKrzyzyk.setVisible(true);
        revalidate();
    }

    private void jMenuItemNowaGraKiKActionPerformed(ActionEvent evt) {
        remove(kolkoKrzyzyk);
        remove(statki);
        remove(symulator);
        kolkoKrzyzyk.setSingleMode(true);
        this.kolkoKrzyzyk = new KolkoKrzyzyk();
        kolkoKrzyzyk.setSingleMode(true);
        add(kolkoKrzyzyk, BorderLayout.CENTER);
        kolkoKrzyzyk.setVisible(true);
        revalidate();
    }

    private void jMenuItemNowaGraSymulatorActionPerformed(ActionEvent evt) {
        try {
            this.symulator = new TextPanel();
        } catch (Exception e) {
            System.out.println("error");
        }
        remove(kolkoKrzyzyk);
        remove(statki);
        remove(symulator);
        add(symulator, BorderLayout.CENTER);
        symulator.setVisible(true);
        revalidate();
    }

    private void zapiszWygladActionPerformed(ActionEvent evt) {
        statki.setShip1(jTexTFieldsetShip1.getText());
        statki.setShip2(jTexTFieldsetShip2.getText());
        statki.setShip3(jTexTFieldsetShip3.getText());
        statki.setShip4(jTexTFieldsetShip4.getText());
        statki.setShip5(jTexTFieldsetShip5.getText());
        statki.setHit(jTexTFieldsetHit.getText());
        statki.setMiss(jTexTFieldsetMiss.getText());
        statki.setUndefined(jTexTFieldsetUndefined.getText());
        statki.setWater(jTexTFieldsetWater.getText());
        statki.NewGame();
    }

    private void zapiszIloscActionPerformed(ActionEvent evt) {
        statki.setQuantity1((int) jTexTFieldsetQuantity1.getValue());
        statki.setQuantity2((int) jTexTFieldsetQuantity2.getValue());
        statki.setQuantity3((int) jTexTFieldsetQuantity3.getValue());
        statki.setQuantity4((int) jTexTFieldsetQuantity4.getValue());
        statki.setQuantity5((int) jTexTFieldsetQuantity5.getValue());
        statki.setQuantity11((int) jTexTFieldsetQuantity11.getValue());
        statki.setQuantity22((int) jTexTFieldsetQuantity22.getValue());
        statki.setQuantity33((int) jTexTFieldsetQuantity33.getValue());
        statki.setQuantity44((int) jTexTFieldsetQuantity44.getValue());
        statki.setQuantity55((int) jTexTFieldsetQuantity55.getValue());
    }

    private void jMenuItemTabelaKiKActionPerformed(ActionEvent evt) {
        kolkoKrzyzyk.getScore();
    }

    private void jMenuItemDostosujStatkiIloscActionPerformed(ActionEvent evt) {
        System.out.println("jMenuDostosujStatkiActionPerformed ");
        jFrameDostosujStatki = new JFrame("Statki - ustawienia");
        jFrameDostosujStatki.setLayout(new GridLayout(0, 4, 60, 60));
        jFrameDostosujStatki.setPreferredSize(new Dimension(screenSize.width, 600));
        jFrameDostosujStatki.setSize(new Dimension(screenSize.width, 600));
        jFrameDostosujStatki.setBounds(center.x - screenSize.width / 2, center.y - screenSize.height / 3, screenSize.width, screenSize.height * 2 / 3);
        jFrameDostosujStatki.add(new JLabel("Ilość 1 polowych statków gracza"));
        jTexTFieldsetQuantity1 = new JSpinner(new SpinnerNumberModel(1, 0, 5, 1));
        jFrameDostosujStatki.add(jTexTFieldsetQuantity1);
        jFrameDostosujStatki.add(new JLabel("Ilość 1 polowych statków przeciwnika"));
        jTexTFieldsetQuantity11 = new JSpinner(new SpinnerNumberModel(1, 0, 5, 1));
        jFrameDostosujStatki.add(jTexTFieldsetQuantity11);
        ////////
        jFrameDostosujStatki.add(new JLabel("Ilość 2 polowych statków gracza"));
        jTexTFieldsetQuantity2 = new JSpinner(new SpinnerNumberModel(1, 0, 5, 1));
        jFrameDostosujStatki.add(jTexTFieldsetQuantity2);
        jFrameDostosujStatki.add(new JLabel("Ilość 2 polowych statków przeciwnika"));
        jTexTFieldsetQuantity22 = new JSpinner();
        jFrameDostosujStatki.add(jTexTFieldsetQuantity22);
        //////////
        jFrameDostosujStatki.add(new JLabel("Ilość 3 polowych statków gracza"));
        jTexTFieldsetQuantity3 = new JSpinner(new SpinnerNumberModel(1, 0, 5, 1));
        jFrameDostosujStatki.add(jTexTFieldsetQuantity3);
        jFrameDostosujStatki.add(new JLabel("Ilość 3 polowych statków przeciwnika"));
        jTexTFieldsetQuantity33 = new JSpinner(new SpinnerNumberModel(1, 0, 5, 1));
        jFrameDostosujStatki.add(jTexTFieldsetQuantity33);
        /////////
        jFrameDostosujStatki.add(new JLabel("Ilość 4 polowych statków gracza"));
        jTexTFieldsetQuantity4 = new JSpinner(new SpinnerNumberModel(1, 0, 5, 1));
        jFrameDostosujStatki.add(jTexTFieldsetQuantity4);
        jFrameDostosujStatki.add(new JLabel("Ilość 4 polowych statków przeciwnika"));
        jTexTFieldsetQuantity44 = new JSpinner(new SpinnerNumberModel(1, 0, 5, 1));
        jFrameDostosujStatki.add(jTexTFieldsetQuantity44);
        //////////
        jFrameDostosujStatki.add(new JLabel("Ilość 5 polowych statków gracza"));
        jTexTFieldsetQuantity5 = new JSpinner();
        jFrameDostosujStatki.add(jTexTFieldsetQuantity5);
        jFrameDostosujStatki.add(new JLabel("Ilość 5 polowych statków przeciwnika"));
        jTexTFieldsetQuantity55 = new JSpinner(new SpinnerNumberModel(1, 0, 5, 1));
        jFrameDostosujStatki.add(jTexTFieldsetQuantity55);
        jFrameDostosujStatki.add(new JPanel());
        jFrameDostosujStatki.add(przywroc);
        jFrameDostosujStatki.add(zapiszIlosc);
        jTexTFieldsetQuantity1.setValue(statki.getQuantity1());
        jTexTFieldsetQuantity2.setValue(statki.getQuantity2());
        jTexTFieldsetQuantity3.setValue(statki.getQuantity3());
        jTexTFieldsetQuantity4.setValue(statki.getQuantity4());
        jTexTFieldsetQuantity5.setValue(statki.getQuantity5());
        jTexTFieldsetQuantity11.setValue(statki.getQuantity11());
        jTexTFieldsetQuantity22.setValue(statki.getQuantity22());
        jTexTFieldsetQuantity33.setValue(statki.getQuantity33());
        jTexTFieldsetQuantity44.setValue(statki.getQuantity44());
        jTexTFieldsetQuantity55.setValue(statki.getQuantity55());
        jFrameDostosujStatki.setVisible(true);
    }

    private void jMenuItemDostosujStatkiWygladActionPerformed(ActionEvent evt) {
        System.out.println("jMenuDostosujStatkiActionPerformed ");
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0, 2, 0, 40));
        JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayout(0, 2, 0, 40));
        JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayout(0, 2, 0, 0));
        panel.setVisible(true);
        panel2.setVisible(true);
        jFrameDostosujStatki = new JFrame("Statki - ustawienia");
        jFrameDostosujStatki.setLayout(new GridLayout(0, 2, 300, 0));
        jFrameDostosujStatki.setPreferredSize(new Dimension(screenSize.width, 600));
        jFrameDostosujStatki.setSize(new Dimension(screenSize.width, 600));
        jFrameDostosujStatki.setBounds(center.x - screenSize.width / 2, center.y - screenSize.height / 3, screenSize.width, screenSize.height * 2 / 3);
        jFrameDostosujStatki.add(panel);
        jFrameDostosujStatki.add(panel2);
        jTexTFieldsetShip1 = new JTextField();
        jTexTFieldsetWater = new JTextField();
        jTexTFieldsetShip2 = new JTextField();
        jTexTFieldsetUndefined = new JTextField();
        jTexTFieldsetShip3 = new JTextField();
        jTexTFieldsetHit = new JTextField();
        jTexTFieldsetShip4 = new JTextField();
        jTexTFieldsetMiss = new JTextField();
        jTexTFieldsetShip5 = new JTextField();
        jTexTFieldsetShip1.setText(statki.getShip1());
        jTexTFieldsetShip2.setText(statki.getShip2());
        jTexTFieldsetShip3.setText(statki.getShip3());
        jTexTFieldsetShip4.setText(statki.getShip4());
        jTexTFieldsetShip5.setText(statki.getShip5());
        jTexTFieldsetWater.setText(statki.getWater());
        jTexTFieldsetUndefined.setText(statki.getUndefined());
        jTexTFieldsetHit.setText(statki.getHit());
        jTexTFieldsetMiss.setText(statki.getMiss());
        jFrameDostosujStatki.setVisible(true);
        panel.add(new JLabel("Wygląd 1 polowych statków"));
        panel.add(jTexTFieldsetShip1);
        panel.add(new JLabel("Wygląd 2 polowych statków"));
        panel.add(jTexTFieldsetShip2);
        panel.add(new JLabel("Wygląd 3 polowych statków"));
        panel.add(jTexTFieldsetShip3);
        panel.add(new JLabel("Wygląd 4 polowych statków"));
        panel.add(jTexTFieldsetShip4);
        panel.add(new JLabel("Wygląd 5 polowych statków"));
        panel.add(jTexTFieldsetShip5);
        panel.add(new JPanel());
        panel.add(new JPanel());
        panel.add(new JPanel());
        panel2.add(new JLabel("Ustaw wodę"));
        panel2.add(jTexTFieldsetWater);
        panel2.add(new JLabel("Niewiadome pola"));
        panel2.add(jTexTFieldsetUndefined);
        panel2.add(new JLabel("Trafione statki"));
        panel2.add(jTexTFieldsetHit);
        panel2.add(new JLabel("Nietrafione statki"));
        panel2.add(jTexTFieldsetMiss);
        panel2.add(new JPanel());
        panel2.add(new JPanel());
        panel2.add(new JPanel());
        panel2.add(new JPanel());
        panel2.add(zapiszWyglad);
        panel2.add(przywroc);
    }

    private void jMenuItemDostosujSudokuActionPerformed(ActionEvent evt) {
        System.out.println("jMenuItemDostosujSudokuActionPerformed ");
        JFrame jFrameDostosujSudoku = new JFrame("Sudoku - ustawienia");
        jFrameDostosujSudoku.setLayout(new GridLayout(5, 3));
    }

    private void jMenuItemDostosujKiKZnakActionPerformed(ActionEvent evt) {
        System.out.println("jMenuItemDostosujKiKActionPerformed");
        kolkoKrzyzyk.setSigns();
    }

    private void jMenuItemDostosujKiKNazwaActionPerformed(ActionEvent evt) {
        System.out.println("jMenuItemDostosujKiKActionPerformed");
        kolkoKrzyzyk.setNames();
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
        symulator.setfacebean(false);
        jTextArea1.append("FaceBean wyłączony\n");
    }

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
        jTextArea1.append("temperatura wynosi " + symulator.gettemperatura() + " °C\n");
        jTextArea1.append("temperatura gorna blad wynosi " + symulator.gettemperaturaGornaBlad() + " °C\n");
        jTextArea1.append("temperatura gorna wynosi " + symulator.gettemperaturaGorna() + " °C\n");
        jTextArea1.append("temperatura dolna wynosi " + symulator.gettemperaturaDolna() + " °C\n");
        jTextArea1.append("temperatura dolna blad wynosi " + symulator.gettemperaturaDolnaBlad() + " °C\n");
    }

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
        symulator.settemperatura(new Random().nextFloat() * 30 + 25);        // TODO add your handling code here:
        symulator.settemperatura((symulator.gettemperatura() * 10.0));
        symulator.settemperatura(Math.round(symulator.gettemperatura()));
        symulator.settemperatura((symulator.gettemperatura() / 10.0));
        jTextArea1.append("Ustawiono temperaturę na " + symulator.gettemperatura() + " °C\n");
        System.out.println(symulator.gettemperatura());
    }

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {
        symulator.setfacebean(true);
        jTextArea1.append("FaceBean włączony\n");
    }

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {
        symulator.settemperaturaGornaBlad((float) Double.parseDouble(jSpinner1.getValue().toString()));        // TODO add your handling code here:
        jTextArea1.append("Ustawiono temperatura górna błąd na " + jSpinner1.getValue().toString() + " °C\n");
    }

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {
        symulator.settemperaturaGorna((float) Double.parseDouble(jSpinner2.getValue().toString()));        // TODO add your handling code here:
        jTextArea1.append("Ustawiono temperatura górna  na " + jSpinner2.getValue().toString() + " °C\n");
    }

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {
        symulator.settemperaturaDolna((float) Double.parseDouble(jSpinner3.getValue().toString()));        // TODO add your handling code here:
        jTextArea1.append("Ustawiono temperatura dolna " + jSpinner3.getValue().toString() + " °C\n");
    }

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {
        symulator.settemperaturaDolnaBlad((float) Double.parseDouble(jSpinner4.getValue().toString()));        // TODO add your handling code here:
        jTextArea1.append("Ustawiono temperatura dolna błąd na " + jSpinner4.getValue().toString() + " °C\n");
    }

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {
        symulator.settemperaturaDolnaBlad(32);
        symulator.settemperaturaGornaBlad(42);
        symulator.settemperaturaDolna(36);
        symulator.settemperaturaGorna((float) 37.5);        // TODO add your handling code here:
        symulator.setfacebean(false);
        jSpinner1.setValue(symulator.gettemperaturaDolnaBlad());
        jSpinner2.setValue(symulator.gettemperaturaGornaBlad());
        jSpinner3.setValue(symulator.gettemperaturaDolna());
        jSpinner4.setValue(symulator.gettemperaturaGorna());
        jSpinner5.setValue(symulator.gettemperatura());
        jTextArea1.append("Przywrócono domyślne zakresy temperatur\n");
    }

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {
        symulator.settemperatura((float) Double.parseDouble(jSpinner5.getValue().toString()));        // TODO add your handling code here:
        jTextArea1.append("Ustawiono temperaturę na " + jSpinner5.getValue().toString() + " °C\n");
    }

    private void jMenuItemDostosujSymulatorActionPerformed(ActionEvent evt) {
        System.out.println("jMenuDostosujStatkiActionPerformed ");
        JPanel panel3 = new JPanel();
        JPanel panel2 = new JPanel();
        panel3.setLayout(new GridLayout(0, 3, 0, 0));
        panel2.setLayout(new GridLayout(1, 0, 0, 0));
        JFrame jFrameDostosujSymulator = new JFrame("Symulator 2020/2021 - ustawienia");
        jFrameDostosujSymulator.setLayout(new GridLayout(0, 1, 0, 0));
        jFrameDostosujSymulator.setPreferredSize(new Dimension(screenSize.width, 600));
        jFrameDostosujSymulator.setSize(new Dimension(screenSize.width, 600));
        jFrameDostosujSymulator.setBounds(center.x - screenSize.width / 2, center.y - screenSize.height / 3, screenSize.width, screenSize.height * 2 / 3);
        jFrameDostosujSymulator.add(panel2);
        jFrameDostosujSymulator.add(panel3);
        panel2.add(jTextArea1);
        panel3.add(jButton4);
        panel3.add(jButton5);
        panel3.add(jSpinner1);
        panel3.add(jButton1);
        panel3.add(jButton6);
        panel3.add(jSpinner2);
        panel3.add(jButton2);
        panel3.add(jButton7);
        panel3.add(jSpinner3);
        panel3.add(jButton3);
        panel3.add(jButton8);
        panel3.add(jSpinner4);
        panel3.add(jButton9);
        panel3.add(jButton10);
        panel3.add(jSpinner5);
        jFrameDostosujSymulator.setVisible(true);
    }
}

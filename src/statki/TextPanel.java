package statki;

import javax.swing.*;
import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * @author adam8
 */
public final class TextPanel extends JPanel implements Serializable {

    private final JTextArea textArea;
    private final JTextArea textArea2;
    private final String kom_niska_temperatua = "Masz za niską temperaturę\n";
    private final String kom_wysoka_temperatua = "Masz za wysoką temperaturę\n";
    private final String kom_ok_temperatua = "Masz odpowiednią temperaturę\n";
    private final String za_niska = "błąd, za niska temperatura\n";
    private final String za_wysoka = "błąd, za wysoka temperatura\n";
    private final JButton przycisk;
    private final JButton przycisk_wstecz;
    private double temperatura;
    private float temperaturaDolnaBlad;
    private float temperaturaGornaBlad;
    private float temperaturaDolna;
    private float temperaturaGorna;
    private JPanel panel;
    private JPanel panel2;
    private String imie = "";
    private String nazwisko = "";
    private String stan = "";
    private FaceBean facebean;


    public TextPanel() throws InterruptedException {
        temperaturaDolnaBlad = 32;
        temperaturaGornaBlad = 42;
        temperaturaDolna = 36;
        temperaturaGorna = (float) 37.5;
        try {
            facebean = new FaceBean();
        } catch (Exception e) {
            System.out.println("brak facebean");
        }
        przycisk = new JButton();
        przycisk_wstecz = new JButton("cofnij");
        textArea = new JTextArea();
        textArea2 = new JTextArea();
        JPanel panel = new JPanel();
        JPanel panel2 = new JPanel();
        panel.setVisible(true);
        setLayout(new GridLayout(1, 0));
        add(panel2);
        try {
            add(facebean);
        } catch (Exception e) {
            System.out.println("brak facebean");
        }
        panel2.setLayout(new GridLayout(3, 0));
        panel2.add(textArea);
        panel2.add(textArea2);
        panel2.add(panel);
        panel.add(new JScrollPane(przycisk), BorderLayout.PAGE_END);
        panel.add(new JScrollPane(przycisk_wstecz), BorderLayout.PAGE_START);
        przycisk.setText("dalej");
        start();
        todo();
    }

    void start() throws InterruptedException {
        Random rand = new Random();
        settemperatura(rand.nextFloat() * 30 + 25);                                // 25-55
        settemperatura((gettemperatura() * 10.0));
        settemperatura(Math.round(gettemperatura()));
        settemperatura((gettemperatura() / 10.0));
        System.out.println(" temperatura " + gettemperatura());
        imie = "";
        nazwisko = "";
        textArea.setText("Witam w aplikacji ''Bramka Termiczna''\n");
        textArea.append("kliknij przycisk by przejść dalej");
        textArea.setEditable(false);
        textArea2.setEditable(false);
        textArea2.setText("");
        try {
            facebean.setVisible(false);
        } catch (Exception e) {
            System.out.println("brak facebean");
        }
    }

    void sprawdz() throws InterruptedException {
        settemperatura((gettemperatura() * 10.0));
        settemperatura(Math.round(gettemperatura()));
        settemperatura((gettemperatura() / 10.0));
        try {
            facebean.setVisible(true);
        } catch (Exception e) {
            System.out.println("brak facebean");
        }
        if (gettemperatura() == 0) {
            textArea.append("\n" + "nie pobrano temperatury");
            Thread.sleep(4000);
            stan = "nie pobrano";
        } else {
            if (gettemperatura() < gettemperaturaDolnaBlad()) {
                textArea.append("\n" + za_niska + gettemperatura() + " °C");
                stan = "błąd";
                facebean.nosmile();
                facebean.setColor(Color.white);
            } else if (gettemperatura() > gettemperaturaGornaBlad()) {
                textArea.append("\n" + za_wysoka + gettemperatura() + " °C");
                stan = "błąd";
                facebean.nosmile();
                facebean.setColor(Color.white);
            } else if (gettemperatura() >= gettemperaturaDolnaBlad() && gettemperatura() <= gettemperaturaDolna()) {
                textArea.append("\n" + kom_niska_temperatua + gettemperatura() + " °C");
                try {
                    facebean.frown();
                    facebean.setColor(Color.blue);
                } catch (Exception e) {
                    System.out.println("brak facebean");
                }
                stan = "!!!!!!!!!!";
            } else if (gettemperatura() >= gettemperaturaGorna() && gettemperatura() <= gettemperaturaGornaBlad()) {
                textArea.append("\n" + kom_wysoka_temperatua + gettemperatura() + " °C");
                try {
                    facebean.frown();
                    facebean.setColor(Color.red);
                } catch (Exception e) {
                    System.out.println("brak facebean");
                }
                stan = "!!!!!!!!!!";
            } else if (gettemperatura() > gettemperaturaDolna() && gettemperatura() < gettemperaturaGorna()) {
                textArea.append("\n" + kom_ok_temperatua + gettemperatura() + " °C");
                try {
                    facebean.setColor(Color.green);
                    facebean.smile();
                } catch (Exception e) {
                    System.out.println("brak facebean");
                }
                stan = "ok";
            }
        }
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            try (FileWriter Zapis = new FileWriter(nazwisko + "_" + imie + ".txt", true)) {
                Zapis.append(temperatura + "      " + formatter.format(date) + "      " + stan + "\n");
            }
        } catch (IOException e) {
            System.out.println("błąd zapisu");
        }
        textArea2.setEditable(false);
        // 25-55
    }

    public void settemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double gettemperatura() {
        return temperatura;
    }

    public void settemperaturaDolnaBlad(float temperaturaDolnaBlad) {
        this.temperaturaDolnaBlad = temperaturaDolnaBlad;
    }

    public double gettemperaturaDolnaBlad() {
        return temperaturaDolnaBlad;
    }

    public void settemperaturaGornaBlad(float temperaturaGornaBlad) {
        this.temperaturaGornaBlad = temperaturaGornaBlad;
    }

    public double gettemperaturaGornaBlad() {
        return temperaturaGornaBlad;
    }

    public void settemperaturaDolna(float temperaturaDolna) {
        this.temperaturaDolna = temperaturaDolna;
    }

    public double gettemperaturaDolna() {
        return temperaturaDolna;
    }

    public void settemperaturaGorna(float temperaturaGorna) {
        this.temperaturaGorna = temperaturaGorna;
    }

    public double gettemperaturaGorna() {
        return temperaturaGorna;
    }


    public void setimie(String imie) {
        this.imie = imie;
    }

    public String getimie() {
        return imie;
    }

    public void setnazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getnazwisko() {
        return nazwisko;
    }

    public void setfacebean(boolean onoff) {
        if (onoff == false) {
            facebean.setVisible(false);
        } else if (onoff == true) {
            facebean.setVisible(true);
        }
    }


    private void przyciskActionPerformed(java.awt.event.ActionEvent evt) {
        textArea2.setEditable(true);
        textArea.setText("Wprowadź imię");
        setimie(textArea2.getText());
        System.out.println("imię " + imie);
        if (!imie.equals("")) {
            textArea2.setText("");
            textArea.setText(" twoje imię to " + imie + " Wprowadź nazwisko");
        }
    }

    private void przyciskActionPerformed2(java.awt.event.ActionEvent evt) throws InterruptedException {
        textArea2.setEditable(true);
        setnazwisko(textArea2.getText());
        System.out.println("nazwisko " + nazwisko);
        if (!nazwisko.equals("")) {
            textArea2.setText("");
            textArea.setText("Cześć " + imie + " " + nazwisko);
            textArea.setText("Trwa sprawdzanie Twojej temperatury");
            sprawdz();
        }
    }

    private void przycisk_wsteczActionPerformed(java.awt.event.ActionEvent evt) throws InterruptedException {
        start();
    }

    void todo() {
        przycisk.addActionListener((java.awt.event.ActionEvent evt) -> {
            if (imie.equals("")) {
                przyciskActionPerformed(evt);
            } else if (nazwisko.equals("")) {
                textArea.setText("twoje imię to " + imie + " Wprowadź nazwisko");
                setnazwisko(textArea2.getText());
                try {
                    przyciskActionPerformed2(evt);
                } catch (InterruptedException ex) {
                    System.out.println("błąd");
                }
            } else {
                textArea.setText("Cześć " + imie + " " + nazwisko);
                try {
                    sprawdz();
                } catch (InterruptedException ex) {
                    System.out.println("błąd");
                }
            }
        });
        przycisk_wstecz.addActionListener((java.awt.event.ActionEvent evt) -> {
            try {
                przycisk_wsteczActionPerformed(evt);
            } catch (InterruptedException ex) {
                Logger.getLogger(TextPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void setColor(String Color) {
        switch (Color) {
            case "Black":
                textArea.setForeground(new Color(200, 200, 200));
                textArea.setBackground(new Color(0, 0, 0));
                textArea2.setForeground(new Color(200, 200, 200));
                textArea2.setBackground(new Color(0, 0, 0));
                //  setBackground(new Color(0, 0, 0));
                //  setForeground(new Color(0, 0, 0));
                //  Info.setForeground(new Color(255, 255, 255));
                //  Info.setBackground(new Color(0, 0, 0));
                break;
            case "White":
                textArea.setForeground(new Color(0, 0, 0));
                textArea.setBackground(new Color(255, 255, 255));
                textArea2.setForeground(new Color(0, 0, 0));
                textArea2.setBackground(new Color(255, 255, 255));
                //   setBackground(new Color(255, 255, 255));
                //  setForeground(new Color(255, 255, 255));
                //  Info.setForeground(new Color(0, 0, 0));
                // Info.setBackground(new Color(255, 255, 255));
                break;
        }
    }
}

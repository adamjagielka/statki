package statki;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public final class KolkoKrzyzyk extends JPanel {

    private final File scoreTable;
    private final ArrayList scoreList;
    private boolean singleMode;
    private JTable panelPlayer;
    private JTextArea info;
    private int scoreA;
    private int scoreB;
    private int playerTurn;
    private boolean win;
    private boolean computerMove;
    private String scoreString;
    private String xName;
    private String oName;
    private String xSign;
    private String oSign;

    public KolkoKrzyzyk() {
        initComponents();
        setColor("White");
        info.setText("X's turn.");
        singleMode = true;
        scoreA = 0;
        scoreB = 0;
        playerTurn = 0;
        xName = "";
        oName = "Bot";
        xSign = "X";
        oSign = "O";
        scoreTable = new File("./scoreTable.txt");
        scoreList = new ArrayList();
        scoreString();
    }

    private void initComponents() {
        info = new JTextArea();
        panelPlayer = new JTable();
        setLayout(new GridLayout(1, 2));
        JScrollPane sp = new JScrollPane(info);
        add(panelPlayer);
        add(sp);
        panelPlayer.setModel(new DefaultTableModel(
                new Object[][]{
                        {"", "", ""},
                        {"", "", ""},
                        {"", "", ""}},
                new String[]{
                        "Title 1", "Title 2", "Title 3"
                }
        ) {
            final boolean[] canEdit = new boolean[]{
                    false, false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        panelPlayer.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                panelPlayerMouseClicked();
            }
        });
        panelPlayer.setShowGrid(true);
        panelPlayer.setFont(new Font("Arial", Font.BOLD, 60));
        panelPlayer.setRowHeight(100);
        TableColumnModel columnModel = panelPlayer.getColumnModel();
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        for (int i = 0; i < 3; i++) {
            columnModel.getColumn(i).setPreferredWidth(100);
            columnModel.getColumn(i).setCellRenderer(centerRenderer);
        }
        panelPlayer.setRowSelectionAllowed(false);
        info.setEditable(false);
    }

    private void panelPlayerMouseClicked() {
        int Column = panelPlayer.getSelectedColumn();
        int Row = panelPlayer.getSelectedRow();
        if (panelPlayer.getValueAt(Row, Column).equals("") && playerTurn < 9 && win == false) {
            if (playerTurn % 2 == 0) {
                panelPlayer.setValueAt(xSign, Row, Column);
                scoreA = scoreA + 10;
                checkResult(xSign);
                if (win == false && playerTurn != 8) {
                    if (oName.equals(""))
                        info.append("\n" + oSign + "'s turn.");
                    else
                        info.append("\n" + oName + "'s turn");
                }
            } else if (playerTurn % 2 == 1) {
                panelPlayer.setValueAt(oSign, Row, Column);
                scoreB = scoreB + 10;
                checkResult(oSign);
                if (win == false && playerTurn != 8) {
                    if (xName.equals(""))
                        info.append("\n" + xSign + "'s turn");
                    else
                        info.append("\n" + xName + "'s turn");
                }
            }
            playerTurn++;
            if (playerTurn == 9 && win == false) {
                scoreA = scoreA + 50;
                scoreB = scoreB + 50;
                info.append("\nIt's a draw!");
                scoreSave();
            } else if (singleMode) {
                computerTurn();
            }
        }
    }

    private void checkResult(String Player) {
        if (panelPlayer.getValueAt(1, 1).equals(Player)) {
            if ((panelPlayer.getValueAt(0, 0).equals(Player) && panelPlayer.getValueAt(2, 2).equals(Player)) ||
                    (panelPlayer.getValueAt(1, 0).equals(Player) && panelPlayer.getValueAt(1, 2).equals(Player)) ||
                    (panelPlayer.getValueAt(2, 0).equals(Player) && panelPlayer.getValueAt(0, 2).equals(Player)) ||
                    (panelPlayer.getValueAt(0, 1).equals(Player) && panelPlayer.getValueAt(2, 1).equals(Player))) {
                win(Player);
            }
        } else if (panelPlayer.getValueAt(0, 0).equals(Player)) {
            if ((panelPlayer.getValueAt(0, 1).equals(Player) && panelPlayer.getValueAt(0, 2).equals(Player)) ||
                    (panelPlayer.getValueAt(1, 0).equals(Player) && panelPlayer.getValueAt(2, 0).equals(Player))) {
                win(Player);
            }
        } else if (panelPlayer.getValueAt(2, 2).equals(Player)) {
            if ((panelPlayer.getValueAt(2, 0).equals(Player) && panelPlayer.getValueAt(2, 1).equals(Player)) ||
                    (panelPlayer.getValueAt(0, 2).equals(Player) && panelPlayer.getValueAt(1, 2).equals(Player))) {
                win(Player);
            }
        }
    }

    private void win(String Player) {
        win = true;
        if (Player.equals(xSign)) {
            scoreA = scoreA + 100;
            if (xName.equals(""))
                info.append("\n" + xSign + "wins! Congratulations!");
            else
                info.append("\n" + xName + " wins! Congratulations!");
        } else if (Player.equals(oSign)) {
            scoreB = scoreB + 100;
            if (oName.equals(""))
                info.append("\n" + oSign + "wins! Congratulations!");
            else
                info.append("\n" + oName + " wins! Congratulations!");
        }
        scoreSave();
    }

    private void scoreString() {
        scoreList.clear();
        scoreString = "";
        try (BufferedReader br = new BufferedReader(new FileReader(scoreTable))) {
            String line;
            while ((line = br.readLine()) != null) {
                scoreList.add(line);
            }
        } catch (FileNotFoundException ex) {
            try {
                scoreTable.createNewFile();
            } catch (IOException ex1) {
                info.append("\nProblem with creating Score File.");
            }
        } catch (IOException ex) {
            info.append("\nProblem with reading Score File.");
        }
        Collections.sort(scoreList, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return extractInt(o2) - extractInt(o1);
            }

            int extractInt(String s) {
                String num = s.substring(0, 3);
                String num2 = num.replaceAll("\\D", "");
                return num2.isEmpty() ? 0 : Integer.parseInt(num2.trim());
            }
        });
        if (scoreList.size() > 10) {
            for (int i = scoreList.size() - 1; i > 9; i--) {
                scoreList.remove(i);
            }
        }
        if (scoreList.size() > 0) {
            for (int i = 0; i < scoreList.size(); i++) {
                scoreString = scoreString + (i + 1) + ". " + scoreList.get(i) + "\n";
            }
        } else scoreString = "There's no scores!";
    }

    private void scoreSave() {
        setNames();
        info.append("\n" + xName + " - " + scoreA + "points\n" + oName + " - " + scoreB + "points");
        scoreList.add(scoreA + "    " + xName);
        scoreList.add(scoreB + "    " + oName);
        String s = "";
        for (int i = 0; i < scoreList.size(); i++) {
            s = s + scoreList.get(i) + "\n";
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(scoreTable))) {
            writer.write(s);
        } catch (IOException ex) {
            info.append("\nProblem with saving Score File.");
        }
        scoreString();
    }

    private void computerTurn() {
        computerMove = false;
        check("o");
        if (!computerMove)
            check("x");
    }

    private void check(String player) {
        int count = 0;
        int empty = 0;
        int emptyCol = -1;
        int emptyRow = -1;
        String sign;
        if (player.equals("o"))
            sign = oSign;
        else
            sign = xSign;
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                if (c == r) {
                    if (panelPlayer.getValueAt(r, c).equals(sign))
                        count++;
                    if (panelPlayer.getValueAt(r, c).equals("")) {
                        empty++;
                        emptyCol = c;
                        emptyRow = r;
                    }
                }
            }
        }
        if (count == 2 && empty == 1) {
            computerMove(emptyRow, emptyCol);
        } else {
            count = 0;
            empty = 0;
            for (int r = 0; r < 3; r++) {
                for (int c = 0; c < 3; c++) {
                    if (c - r == 2 || r - c == 2 || c + r == 2) {
                        if (panelPlayer.getValueAt(r, c).equals(sign))
                            count++;
                        if (panelPlayer.getValueAt(r, c).equals("")) {
                            empty++;
                            emptyCol = c;
                            emptyRow = r;
                        }
                    }
                }
            }
            if (count == 2 && empty == 1) {
                computerMove(emptyRow, emptyCol);
            } else {
                count = 0;
                empty = 0;
                for (int r = 0; r < 3; r++) {
                    for (int c = 0; c < 3; c++) {
                        if (panelPlayer.getValueAt(r, c).equals(sign))
                            count++;
                        if (panelPlayer.getValueAt(r, c).equals("")) {
                            empty++;
                            emptyCol = c;
                            emptyRow = r;
                        }
                    }
                    if (count == 2 && empty == 1) {
                        computerMove(emptyRow, emptyCol);
                    } else {
                        count = 0;
                        empty = 0;
                    }
                }
                if (!computerMove) {
                    for (int c = 0; c < 3; c++) {
                        for (int r = 0; r < 3; r++) {
                            if (panelPlayer.getValueAt(r, c).equals(sign))
                                count++;
                            if (panelPlayer.getValueAt(r, c).equals("")) {
                                empty++;
                                emptyCol = c;
                                emptyRow = r;
                            }
                        }
                        if (count == 2 && empty == 1) {
                            computerMove(emptyRow, emptyCol);
                        } else {
                            count = 0;
                            empty = 0;
                        }
                    }
                }
                if (emptyRow != -1 && emptyCol != -1 && computerMove == false && player.equals("x")) {
                    computerMove(emptyRow, emptyCol);
                } else if (player.equals("x") && computerMove == false) {
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < 3; j++) {
                            if (panelPlayer.getValueAt(i, j).equals("") && computerMove == false) {
                                computerMove(i, j);
                            }
                        }
                    }
                }
            }
        }
    }


    private void computerMove(int row, int column) {
        computerMove = true;
        panelPlayer.setValueAt(oSign, row, column);
        scoreB = scoreB + 10;
        checkResult(oSign);
        if (win == false && playerTurn != 8) {
            if (xName.equals(""))
                info.append("\n" + xSign + "'s turn.");
            else
                info.append("\n" + xName + "'s turn");
        }
        playerTurn++;
        if (playerTurn == 9 && win == false) {
            scoreA = scoreA + 50;
            scoreB = scoreB + 50;
            info.append("\nIt's a draw!");
            scoreSave();
        }
    }

    public void setColor(String Color) {
        switch (Color) {
            case "Black": {
                info.append("\nChanged color to black");
                panelPlayer.setForeground(new java.awt.Color(0, 200, 0));
                panelPlayer.setBackground(new java.awt.Color(0, 0, 0));
                info.setForeground(new java.awt.Color(255, 255, 255));
                info.setBackground(new java.awt.Color(0, 0, 0));
            }
            case "White": {
                info.append("\nChanged color to white");
                panelPlayer.setForeground(new java.awt.Color(0, 200, 0));
                panelPlayer.setBackground(new java.awt.Color(255, 255, 255));
                info.setForeground(new java.awt.Color(0, 0, 0));
                info.setBackground(new java.awt.Color(255, 255, 255));
            }
            default:
                info.append("\nError, color not found");
        }
    }

    public void newGame() {
        scoreA = 0;
        scoreB = 0;
        playerTurn = 0;
        win = false;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                panelPlayer.setValueAt("", i, j);
            }
        }
        if (xName.equals(""))
            info.setText("Started new game! \n" + xSign + "'s turn.");
        else
            info.setText("Started new game! \n" + xName + "'s turn.");
    }

    public void getScore() {
        info.append("\nBest scores:\n" + scoreString);
    }

    public void setNames() {
        String x = (String) JOptionPane.showInputDialog(null, "Player " + xSign + ", What's Your name?", "Change Name", JOptionPane.QUESTION_MESSAGE, null, null, xName);
        if (x != null) xName = x;
        if (xName.equals("")) xName = xSign;
        String o;
        if (!singleMode)
            o = (String) JOptionPane.showInputDialog(null, "Player " + oSign + ", What's Your name?", "Change Name", JOptionPane.QUESTION_MESSAGE, null, null, oName);
        else
            o = (String) JOptionPane.showInputDialog(null, "Choose " + oSign + "'s name.", "Change Name", JOptionPane.QUESTION_MESSAGE, null, null, oName);
        if (o != null) oName = o;
        if (oName.equals("")) oName = oSign;
    }

    public void setSigns() {
        String xMessage;
        String oMessage;
        if (xName.equals(""))
            xMessage = "Player " + xSign + ", choose Your sign: ";
        else
            xMessage = xName + ", choose Your sign: ";
        if (singleMode) {
            oMessage = "Choose " + oName + "'s sign: ";
        } else if (oName.equals(""))
            oMessage = "Player " + oSign + ", choose Your sign: ";
        else
            oMessage = oName + ", choose Your sign: ";
        String[] xChoices = {"X", "☺", "♥", "♣", "♂", "♪"};
        String[] oChoices = {"O", "☻", "♦", "♠", "♀", "♫"};
        String x = (String) JOptionPane.showInputDialog(null, xMessage, "Change Sign", JOptionPane.QUESTION_MESSAGE, null, xChoices, xSign);
        if (x != null) {
            if (xName.equals(xSign)) xName = x;
            xSign = x;
        }
        String o = (String) JOptionPane.showInputDialog(null, oMessage, "Change Sign", JOptionPane.QUESTION_MESSAGE, null, oChoices, xSign);
        if (o != null) {
            if (oName.equals(oSign)) oName = o;
            oSign = o;
        }
        if (playerTurn != 0)
            newGame();
    }

    public void setSingleMode(boolean mode) {
        singleMode = mode;
        if (!mode && oName.equals("Bot")) {
            oName = "";
        }
        newGame();
        if (mode)
            info.append("\nChanged mode to single.");
        else
            info.append("\nChanged mode to multi.");
    }
}
